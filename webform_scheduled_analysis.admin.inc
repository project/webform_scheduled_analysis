<?php

/**
 * Entity form for the scheduled analysis entity.
 */
function webform_scheduled_analysis_form($form, &$form_state, $schedule_analysis_config_id, $node) {
  $config = WebformScheduledAnalysisConfig::loadOrCreate($schedule_analysis_config_id);
  $form_state['nid'] = $node->nid;

  module_load_include('inc', 'webform', 'includes/webform.report');

  $form['schedule_info'] =
    array(
    '#title' => t('Schedule Analysis'),
    '#type' => 'fieldset',
    '#weight' => -2,
  );
  $form['schedule_info']['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#description' => t('Disable or Enable this config without losing your scheduled configuration'),
    '#default_value' => $config->isEnabled(),
  );
  $form['schedule_info']['email'] = array(
    '#type' => 'textarea',
    '#title' => t('Email'),
    '#default_value' => $config->getEmail(),
    '#required' => TRUE,
    '#description' => t('You can enter multiple emails, one per line.'),
  );

  $form['schedule_info']['send_date'] = array(
    '#type' => 'date_popup',
    '#title' => t('Next Send Date'),
    '#description' => $config->isDelivered() ? t('This email has already been sent') : t('This email will be sent on the next cron after this date passes.'),
    '#default_value' => date('Y-m-d H:i', $config->getSendDate()),
    '#date_type' => DATE_UNIX,
    '#date_timezone' => date_default_timezone(),
    '#date_format' => variable_get('webform_scheduled_analysis_date_display_format', 'd/m/Y H:i'),
    '#date_increment' => 15,
    '#required' => TRUE,
  );
  $form['schedule_info']['limit_to_new'] = array(
    '#type' => 'checkbox',
    '#title' => t('Limit to new submissions'),
    '#description' => t('Check this box to ensure the analysis only includes data from since the last time the analysis email was sent.'),
    '#default_value' => $config->getLimitToNew(),
  );
  $form['schedule_info']['frequency'] = array(
    '#type' => 'select',
    '#title' => t('Frequency'),
    '#description' => t('Should this scheduled analysis repeat?'),
    '#default_value' => $config->getFrequency(),
    '#options' => array(
      'once' => t('Send Once'),
      'daily' => t('Daily'),
      'weekly' => t('Weekly'),
      'monthly' => t('Monthly'),
    ),
  );

  $export_types = AnalysisExportFormatManager::getDefinitions();
  $form['export_format'] = array(
    '#title' => t('Export format'),
    '#type' => 'select',
    '#options' => array_column($export_types, 'title', 'name'),
    '#default_value' => $config->getExportFormat(),
  );

  module_load_include('inc', 'webform', 'includes/webform.components');
  $component_list = webform_component_list($node, 'analysis', TRUE);
  $is_new = $config->internalIdentifier() === NULL;
  $form['components'] = array(
    '#type' => 'select',
    '#title' => t('Included analysis components'),
    '#options' => $component_list,
    '#default_value' => $is_new ? array_keys($component_list) : $config->getComponents(),
    '#multiple' => TRUE,
    '#size' => 10,
    '#description' => t('The selected components will be included in the analysis.'),
    '#process' => array('webform_component_select'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Schedule'),
  );

  return $form;
}

/**
 * Submit callback for the webform scheduled analysis.
 */
function webform_scheduled_analysis_form_submit($form, $form_state) {
  $config = WebformScheduledAnalysisConfig::loadOrCreate($form_state['build_info']['args'][0]);
  $values = $form_state['values'];

  $send_date = \DateTime::createFromFormat('Y-m-d H:i', $form_state['values']['send_date']);

  $config->setStatus($values['enabled']);
  $config->setEmail($values['email']);
  $config->setSendDate($send_date->getTimestamp());
  $config->setFrequency($values['frequency']);
  $config->setFrequency($values['frequency']);
  $config->setComponents($values['components']);
  $config->setExportFormat($values['export_format']);
  $config->setLimitToNew($values['limit_to_new']);
  $config->setWebformNid($form_state['nid']);

  // Always resend an analysis if the form is resaved.
  $config->setDelivered(FALSE);

  $config->save();
}
