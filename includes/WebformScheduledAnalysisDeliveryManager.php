<?php

/**
 * A class to manage the delivery of scheduled analysis.
 */
class WebformScheduledAnalysisDeliveryManager {

  /**
   * Implements hook_cron().
   */
  public function cron() {

    $analysis_schedules = \WebformScheduledAnalysisConfig::getAnalysisReadyToSend();

    foreach ($analysis_schedules as $analysis_schedule) {

      // Get the export type, node and analysis.
      $analysis_export_type = AnalysisExportFormatManager::createInstance($analysis_schedule->getExportFormat());
      $node = node_load($analysis_schedule->getWebformId());

      // Check if we need to filter by last send date.
      $query = NULL;
      if ($analysis_schedule->getLimitToNew()) {
        $query = db_select('webform_submissions', 'aws')
          ->fields('aws', array('sid', 'submitted'))
          ->condition('aws.submitted', $analysis_schedule->getLastDelivery(), '>');;
      }

      $analysis = webform_analysis_export_get_webform_analysis_data($node, $analysis_schedule->getComponents(), $query);

      // Add the analysis to a temp file.
      $file = fopen('php://temp', 'r+');
      $analysis_export_type->getContent($analysis, $file, $node);
      rewind($file);
      $file_contents = stream_get_contents($file);

      // Send an email to everyone on the schedule.
      foreach (explode(PHP_EOL, $analysis_schedule->getEmail()) as $mail) {
        drupal_mail('webform_scheduled_analysis', 'scheduled_analysis', trim($mail), language_default(), array(
          'context' => array(
            'node' => $node,
          ),
          'attachments' => array(
            array(
              'filecontent' => $file_contents,
              'filename' => sprintf('webform-analysis.%s', $analysis_export_type->getFileExtension()),
              'filemime' => $analysis_export_type->getContentType(),
            ),
          ),
        ));
      }

      // Mark the email as delivered so the schedule can be updated for the next
      // time it should be sent.
      $analysis_schedule->markDelivered();
    }
  }

  /**
   * Implements hook_mail().
   */
  public function mail($key, &$message, $params) {
    if ($key !== 'scheduled_analysis') {
      return;
    }

    $node = $params['context']['node'];

    $message['subject'] = t('Webform analysis of "@title"', array('@title' => $node->title));
    $message['body'][] = t('The analysis of the "@title" webform are attached.', array('@title' => $node->title));

    if (!empty($params['attachments'])) {
      $message['params']['attachments'] = $params['attachments'];
    }
  }

}
