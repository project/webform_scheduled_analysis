<?php

/**
 * The configuration entity for storing analysis analysis config.
 */
class WebformScheduledAnalysisConfig extends \Entity {

  const ENABLED = 1;
  const DISABLED = 0;

  public $id;
  public $webform_nid;
  public $status;
  public $send_date;
  public $frequency;
  public $email;
  public $delivered;
  public $components;
  public $format;
  public $last_delivery;
  public $limit_to_new;

  /**
   * Gets a config entity by the webform nid it is related to.
   *
   * @param int $webform_nid
   *   The related webform node id.
   *
   * @return static
   *   The loaded entity if it exists otherwise FALSE.
   */
  public static function loadByWebformNid($webform_nid) {
    return static::loadMultiple(FALSE, array('webform_nid' => $webform_nid));
  }

  /**
   * Load multiple configs via the entity id.
   *
   * @param array|bool $ids
   *   The ids to load.
   * @param array $conditions
   *   The filters or conditions.
   *
   * @return static[]
   *   An array of config entities.
   */
  public static function loadMultiple($ids, $conditions = array()) {
    return entity_load('webform_scheduled_analysis', $ids, $conditions);
  }

  /**
   * Load an existing config entity or create a shell if one does not exist.
   *
   * @param int|null $id
   *   An ID or NULL if the entity should be created.
   *
   * @return \WebformScheduledAnalysisConfig
   *   The loaded config entity.
   */
  public static function loadOrCreate($id) {
    if ($id !== NULL) {
      $entity = entity_load('webform_scheduled_analysis', array($id));
      $entity = array_shift($entity);
    }
    else {
      $entity = WebformScheduledAnalysisConfig::create(array());
    }
    return $entity;
  }

  /**
   * Creates a new config entity.
   *
   * @param array $values
   *   An array of values to create the config with.
   *
   * @return static
   *   The new config entity.
   */
  public static function create(array $values) {
    return entity_create('webform_scheduled_analysis', $values + array(
        'status' => 1,
        'send_date' => REQUEST_TIME,
        'frequency' => 'once',
      ));
  }

  /**
   * Checks if this config is enabled.
   *
   * @return bool
   *   TRUE if it's enabled otherwise FALSE.
   */
  public function isEnabled() {
    return $this->getStatus() === static::ENABLED;
  }

  /**
   * Gets the current config status.
   *
   * @return int
   *   The status as an integer.
   */
  public function getStatus() {
    return (int) $this->status;
  }

  /**
   * Gets the email configuration.
   *
   * @return string
   *   The comma separated emails.
   */
  public function getEmail() {
    return $this->email;
  }

  /**
   * Set the config status.
   *
   * @param bool $status
   *   The status.
   *
   * @return $this
   *   The config object.
   */
  public function setStatus($status) {
    $this->status = (int) $status;
    return $this;
  }

  /**
   * Sets the send date.
   *
   * @param int $send_date
   *   The send date as a Unix timestamp.
   *
   * @return $this
   *   The config object.
   */
  public function setSendDate($send_date) {
    $this->send_date = $send_date;
    return $this;
  }

  /**
   * Sets the available emails.
   *
   * @param string $email
   *   The emails to send to.
   *
   * @return $this
   *   The config object.
   */
  public function setEmail($email) {
    $this->email = $email;
    return $this;
  }

  /**
   * Checks if this config has already been delivered for the next send_date.
   *
   * @return bool
   *   TRUE if already delivered otherwise FALSE.
   */
  public function isDelivered() {
    return (bool) $this->delivered;
  }

  /**
   * Sets whether this analysis has already been sent.
   *
   * @param bool $delivered
   *   The new sent status.
   *
   * @return $this
   *   The config object.
   */
  public function setDelivered($delivered) {
    $this->delivered = (int) $delivered;
    return $this;
  }

  /**
   * Gets the send date for this schedule config.
   *
   * @return int
   *   The send date as a Unix timestamp.
   */
  public function getSendDate() {
    return $this->send_date;
  }

  /**
   * Sets the send frequency.
   *
   * @param string $frequency
   *   The frequency to set, one of: once, daily, weekly or monthly.
   *
   * @return $this
   *   The config object.
   */
  public function setFrequency($frequency) {
    $this->frequency = $frequency;
    return $this;
  }

  /**
   * Gets the config send frequency.
   *
   * @return string
   *   The config frequency.
   */
  public function getFrequency() {
    return $this->frequency;
  }

  /**
   * Set the components.
   *
   * @param array $components
   *   Components to enable for this analysis.
   */
  public function setComponents($components) {
    $this->components = implode(',', array_filter($components));
  }

  /**
   * Get the components.
   *
   * @return array
   *   The components enabled for this analysis.
   */
  public function getComponents() {
    return !empty($this->components) ? explode(',', $this->components) : array();
  }

  /**
   * Sets the webform nid.
   *
   * @param int $webform_nid
   *   The webform nid.
   *
   * @return $this
   *   The config object.
   */
  public function setWebformNid($webform_nid) {
    $this->webform_nid = $webform_nid;
    return $this;
  }

  /**
   * Gets the webform nid.
   *
   * @return int
   *   The associated webform ID.
   */
  public function getWebformId() {
    return $this->webform_nid;
  }

  /**
   * Get the export format.
   *
   * @return string
   *   The export format.
   */
  public function getExportFormat() {
    return $this->format;
  }

  /**
   * Set the export format.
   *
   * @param string $format
   *   The export format.
   *
   * @return $this
   *   The config object.
   */
  public function setExportFormat($format) {
    $this->format = $format;
    return $this;
  }

  /**
   * Set the date the analysis was successfully delivered.
   *
   * @param int $last_delivery
   *   The last delivery date.
   *
   * @return $this
   *   The config object.
   */
  public function setLastDelivery($last_delivery) {
    $this->last_delivery = $last_delivery;
    return $this;
  }

  /**
   * Get the last time the schedule was delivered.
   *
   * @return int
   *   The last time the schedule was delivered.
   */
  public function getLastDelivery() {
    return $this->last_delivery;
  }

  /**
   * Set if we should limit to new submissions.
   *
   * @param boolean $value
   */
  public function setLimitToNew($value) {
    $this->limit_to_new = (int) $value;
  }

  /**
   * Check if we should limit to new submissions.
   *
   * @return bool
   *   If we should limit to new submissions.
   */
  public function getLimitToNew() {
    return (boolean) $this->limit_to_new;
  }

  /**
   * Gets all config entities that are ready to be sent.
   *
   * @return array|static[]
   *   The loaded config entities.
   */
  public static function getAnalysisReadyToSend() {
    $query = new \EntityFieldQuery();
    $result = $query
      ->entityCondition('entity_type', 'webform_scheduled_analysis')
      ->propertyCondition('status', WebformScheduledAnalysisConfig::ENABLED)
      ->propertyCondition('send_date', REQUEST_TIME, '<')
      ->propertyCondition('delivered', 0)
      ->execute();

    if (empty($result['webform_scheduled_analysis'])) {
      return array();
    }
    return static::loadMultiple(array_keys($result['webform_scheduled_analysis']));
  }

  /**
   * Mark a scheduled analysis as sent.
   */
  public function markDelivered() {
    if ($this->getFrequency() === 'once') {
      $this->setDelivered(TRUE);
    }
    else {
      $send_date = $this->getSendDate();

      $one_day = 86400;
      switch ($this->getFrequency()) {
        case 'daily':
          $send_date += $one_day;
          break;

        case 'weekly':
          $send_date += $one_day * 7;
          break;

        case 'monthly':
          $send_date += $one_day * 30;
          break;
      }
      $this->setSendDate($send_date);
      $this->setDelivered(FALSE);
    }
    $this->setLastDelivery(REQUEST_TIME);
    $this->save();
  }

}
