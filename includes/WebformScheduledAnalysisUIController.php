<?php

class WebformScheduledAnalysisUIController extends EntityDefaultUIController {

  public function hook_menu() {
    $path = drupal_get_path('module', 'webform_scheduled_analysis');
    $items['node/%webform_menu/webform-results/scheduled-analysis'] = array(
      'title' => 'Scheduled Analysis',
      'page callback' => 'drupal_get_form',
      'page arguments' => array($this->entityType . '_overview_form', $this->entityType, 1),
      'access callback' => 'webform_results_access',
      'access arguments' => array(1),
      'file' => 'webform_scheduled_analysis.admin.inc',
      'file path' => $path,
      'weight' => 7,
      'type' => MENU_LOCAL_TASK,
    );
    $items['node/%webform_menu/webform-results/scheduled-analysis/add'] = array(
      'title' => 'Scheduled Analysis',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('webform_scheduled_analysis_form', NULL, 1),
      'access callback' => 'webform_results_access',
      'access arguments' => array(1),
      'file' => 'webform_scheduled_analysis.admin.inc',
      'file path' => $path,
      'weight' => 7,
      'type' => MENU_NORMAL_ITEM,
    );
    $items['node/%webform_menu/webform-results/scheduled-analysis/edit/%'] = array(
      'title' => 'Scheduled Analysis',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('webform_scheduled_analysis_form', 5, 1),
      'access callback' => 'webform_results_access',
      'access arguments' => array(1),
      'file' => 'webform_scheduled_analysis.admin.inc',
      'file path' => $path,
      'weight' => 7,
      'type' => MENU_NORMAL_ITEM,
    );
    $items['node/%webform_menu/webform-results/scheduled-analysis/delete/%webform_scheduled_analysis'] = array(
      'title' => 'Scheduled Analysis Delete',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('webform_scheduled_analysis_operation_form', 'webform_scheduled_analysis', 5, 'delete'),
      'load arguments' => array('webform_scheduled_analysis'),
      'access callback' => 'webform_results_access',
      'access arguments' => array(1),
      'file' => 'webform_scheduled_analysis.admin.inc',
      'file path' => $path,
      'weight' => 7,
      'type' => MENU_NORMAL_ITEM,
      'context' => MENU_CONTEXT_INLINE,
    );
    return $items;
  }

  /**
   * Builds the entity overview form.
   */
  public function overviewForm($form, &$form_state) {
    // By default just show a simple overview for all entities.
    $form['table'] = $this->overviewTable(array(), $form_state['build_info']['args'][1]);
    $form['pager'] = array('#theme' => 'pager');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function overviewTable($conditions = array(), $node = NULL) {

    /** @var WebformScheduledAnalysisConfig[] $entities */
    $entities = WebformScheduledAnalysisConfig::loadByWebformNid($node->nid);
    $link_options_return = array('query' => array('destination' => current_path()));

    $output = array(
      'add' => array(
        '#type' => 'link',
        '#title' => t('Create Schedule'),
        '#href' => 'node/' . $node->nid . '/webform-results/scheduled-analysis/add',
        '#attributes' => array(
          'class' => array('button'),
        ),
        '#options' => $link_options_return,
      ),
      'table' => array(
        '#theme' => 'table',
        '#header' => array(
          'Analysis Format',
          'Email',
          'Next Send Date',
          'Frequency',
          'Enabled',
          array(
            'data' => 'Operations',
            'colspan' => 2,
          ),
        ),
      ),
    );

    $export_formats = AnalysisExportFormatManager::getDefinitions();

    if (!empty($entities)) {
      foreach ($entities as $entity) {
        $row = &$output['table']['#rows'][];

        $row[] = $export_formats[$entity->getExportFormat()]['title'];
        $row[] = $entity->getEmail();
        $row[] = date(variable_get('webform_scheduled_analysis_date_display_format', 'd/m/Y H:i'), $entity->getSendDate());
        $row[] = $entity->getFrequency();
        $row[] = $entity->isEnabled() ? t('Yes') : t('No');
        $row[] = l('Edit Schedule', 'node/' . $node->nid . '/webform-results/scheduled-analysis/edit/' . $entity->internalIdentifier(), $link_options_return);
        $row[] = l('Delete Schedule', 'node/' . $node->nid . '/webform-results/scheduled-analysis/delete/' . $entity->internalIdentifier(), $link_options_return);

      }
    }
    else {
      $output['table']['#rows'][] = array(
        array(
          'data' => t('No exports have been scheduled for this webform.'),
          'colspan' => count($output['table']['#header']),
        ),
      );
    }

    return $output;
  }

}
