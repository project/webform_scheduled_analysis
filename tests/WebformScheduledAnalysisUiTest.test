<?php

/**
 * Test the scheduled analysis UI.
 */
class WebformScheduledAnalysisUiTest extends ScheduledAnalysisWebformTestCase {

  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return array(
      'name' => t('Webform Scheduled Analysis Test'),
      'description' => t('Test the scheduled analysis.'),
      'group' => t('Webform Scheduled Analysis'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    return parent::setUp(array(
      'webform',
      'webform_scheduled_analysis',
    ));
  }

  /**
   * Test the UI.
   */
  public function testUi() {

    $node = $this->createWebformNode(array(), array(
      array(
        'name' => 'Select A',
        'type' => 'select',
        'weight' => 1,
        'form_key' => 'select_a',
        'extra' => array(
          'items' => "a|A\nb|B\nc|C",
        ),
      ),
      array(
        'name' => 'Select B',
        'type' => 'select',
        'weight' => 2,
        'form_key' => 'select_b',
        'extra' => array(
          'items' => "d|D\ne|E\nf|F",
        ),
      ),
    ));

    $this->drupalLogin($this->createWebformUser());
    $this->drupalGet('node/' . $node->nid . '/webform-results/analysis');
    $this->clickLink('Scheduled Analysis');
    $this->assertText('No exports have been scheduled for this webform.');
    $this->clickLink('Create Schedule');

    // New schedules can be created.
    $this->drupalPost(NULL, array(
      'enabled' => '1',
      'email' => 'sam@example.com',
      'frequency' => 'weekly',
      'export_format' => 'AnalysisExportFormatCSV',
      'components[1]' => FALSE,
      'components[2]' => TRUE,
      'send_date[date]' => '04/08/2017',
      'send_date[time]' => '05:00',
    ), 'Save Schedule');

    $this->assertText('sam@example.com');
    $this->assertText('CSV');
    $this->assertText('04/08/2017 05:00');
    $this->assertText('sam@example.com');
    $this->assertText('weekly');

    $this->clickLink('Edit Schedule');

    $this->assertNoFieldChecked('edit-components-1');
    $this->assertFieldChecked('edit-components-2');

    $this->drupalPost(NULL, array(
      'enabled' => '0',
      'email' => 'foo@example.com',
      'frequency' => 'daily',
      'export_format' => 'AnalysisExportFormatCSV',
      'components[1]' => TRUE,
      'components[2]' => FALSE,
      'send_date[date]' => '04/07/2017',
      'send_date[time]' => '09:00',
    ), 'Save Schedule');

    $this->clickLink('Delete Schedule');
    $this->drupalPost(NULL, array(), 'Confirm');
    $this->assertText('No exports have been scheduled for this webform.');
  }

}
